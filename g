#! /bin/sh
# -*- scheme -*-
exec ${GUILE-guile} --no-auto-compile -C $(pwd)/out/ccache -L $(pwd) -e '(@@ (gee) main)' -s "$0" ${1+"$@"}
!#

;;; Gee Gee --- Guile-Guided Gee-Guessing
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Gee Gee.
;;;
;;; Gee Gee is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Gee Gee is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gee Gee.  If not, see <http://www.gnu.org/licenses/>.

(read-set! keywords 'prefix)

(define-module (gee)
  :use-module (ice-9 getopt-long)

  :use-module (gee g)
  :use-module (gee misc)
  :use-module (gee config)
  )

(define (parse-opts args)
  (let* ((option-spec
  	  '((help (single-char #\h))
            (verbose (single-char #\v))
            (version (single-char #\V))))
         (options (getopt-long args option-spec
                               :stop-at-first-non-option #t))
         (help? (option-ref options 'help #f))
         (files (option-ref options '() '()))
         (usage? (and (not help?) (null? files)))
         (verbose? (option-ref options 'verbose #f))
         (version? (option-ref options 'version #f)))
    (or (and version?
             (stdout "g (gee gee) ~a\n" VERSION)
             (exit 0))
        (and (or help? usage?)
             ((or (and usage? stderr) stdout) "\
Usage: g [OPTION] [git|guix|guile] [git-command|guix-command|guile-command]

Options:
  -h, --help     display this help and exit
  -v, --verbose  be verbose
  -V, --version  display version information and exit

Examples:
  g build lilypond
  g fetch
  g package -l
  g rebase -i HEAD~~8
  g status
")
             (exit (or (and usage? 2) 0)))
        options)))

(define (main args)
  (let* ((options (parse-opts args))
         (verbose? (option-ref options 'verbose #f))
         (files (option-ref options '() '())))
    (and verbose? (stderr "system* ~a\n" (complete files)))
    (apply system* (complete files))))
