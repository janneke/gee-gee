;;; Gee Gee --- Guile-Guided Gee-Guessing
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Gee Gee.
;;;
;;; Gee Gee is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Gee Gee is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gee Gee.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gee-gee)

  #:use-module (ice-9 optargs)

  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)

  #:use-module (guix download)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix utils)

  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages version-control))

(define-public gee-gee
  (package
    (name "gee-gee")
    (version "0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
    		    "https://github.com/janneke/"
                    name "/archive/v" version ".tar.gz"
                    ))
              (sha256
               (base32 "1kiyb1a93mpjfml57rlizx0bcrw8iw9jabp8cqawq8vg078z06h1"))))
    (build-system gnu-build-system)
    (propagated-inputs `(("guile" ,guile-2.0)))
    (native-inputs `(("git" ,git)
                     ("guile" ,guile-2.0)
                     ("guix" ,guix)
                     ("make" ,gnu-make)))
    (arguments
     `(#:modules ((guix build gnu-build-system)
                  (guix build utils)
                  (gee build))
       #:imported-modules (,@%gnu-build-system-modules
                           (gee build))
       #:phases
       (modify-phases %standard-phases
         (replace 'patch-source-shebangs
                  patch-source-shebangs-no-symlinks))))
    (synopsis "Guile-Guided Gee-Guessing for d33p n173 g17-n-g:iks h4x0rz")
    (description "Gee Gee helps your poor limbic system get to grips
with all these newfangled source and package-based g* commands.")
    (home-page "https://github.com/janneke/gee-gee")
    (license gpl3+)))
