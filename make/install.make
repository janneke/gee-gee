TARBALL_DIR:=$(PACKAGE)-$(VERSION)
TARBALL:=$(OUT)/$(TARBALL_DIR).tar.gz

OPT_CLEAN:=$(OPT_CLEAN) $(TARBALL) .tarball-version

GIT_ARCHIVE_HEAD:=git archive HEAD --
GIT_LS_FILES:=git ls-files
ifeq ($(wildcard .git),)
GIT_ARCHIVE_HEAD:=tar -cf-
GIT_LS_FILES:=find
endif

.tarball-version:
	echo $(COMMIT) > $@

dist: $(TARBALL)

tree-clean-p:
	test ! -d .git || git diff --exit-code > /dev/null
	test ! -d .git || git diff --cached --exit-code > /dev/null
	@echo commit:$(COMMIT)

$(TARBALL): tree-clean-p .tarball-version
	mkdir -p $(OUT)
	($(GIT_LS_FILES) --exclude=$(OUT);\
		echo $^ | grep -Ev 'tree-clean-p' | tr ' ' '\n')\
		| tar --transform=s,^,$(TARBALL_DIR)/,S -T- -czf $@

install:
	mkdir -p  $(DESTDIR)$(PREFIX)/bin
	install g $(DESTDIR)$(PREFIX)/bin/g
	tar -cf- bin/script lib/{README,script.sh} | tar -C $(DESTDIR)$(PREFIX) -xf-
	mkdir -p $(DESTDIR)$(PREFIX)/share/guile/site/$(GUILE_EV)
	$(GIT_ARCHIVE_HEAD) gee\
		| tar -C $(DESTDIR)$(PREFIX)/share/guile/site/$(GUILE_EV) -xf-
	mkdir -p $(DESTDIR)$(PREFIX)/lib/guile/$(GUILE_EV)/ccache
	cd $(OUT) && find ccache -name '*.go'\
		| tar -T- -cf-\
		| tar -C $(DESTDIR)$(PREFIX)/lib/guile/$(GUILE_EV) -xf-

GUIX:=guix
guix: $(TARBALL)
	$(GUIX) build --load-path=. --keep-failed --with-source=$< $(PACKAGE) $(OPTIONS)
	$(MAKE) update-hash

update-hash: $(TARBALL).sha256
	@echo -n hash:
	@cat $<
	sed -i -e 's,(base32 "[^"]*"),(base32 "$(shell cat $<)"),' $(PACKAGE).scm
	! git diff --exit-code
	git commit -m 'RC: guix hash: $(shell cat $<)' $(PACKAGE).scm

$(TARBALL).sha256: $(TARBALL:$(OUT)/%=$(PWD)/$(OUT)/%)
	$(GUIX) download file://$< | tee | tail -1 > $@

CHECKOUT:=$(OUT)/$(PACKAGE)
$(CHECKOUT):
	git clone . $(CHECKOUT)

guix-git: $(CHECKOUT)
	$(GUIX) build --load-path=. --keep-failed --with-source=$< $(PACKAGE)

URL:=https://github.com/janneke/$(PACKAGE)/archive/
MASTER_BALL:=$(OUT)/$(PACKAGE)-0.tar.gz
$(MASTER_BALL):
	@mkdir -p $(@D)
	wget -O $@ $(URL)master.tar.gz

master: $(MASTER_BALL)
	$(GUIX) build -f package.scm --keep-failed --with-source=$< $(OPTIONS)

GITHUB_BALL:=$(OUT)/github/$(PACKAGE)-$(VERSION).tar.gz
$(GITHUB_BALL):
	@mkdir -p $(@D)
	wget -O $@ $(URL)v$(VERSION).tar.gz

release: tree-clean-p check dist build
	git tag v$(VERSION)
	git push --tags origin master
	$(MAKE) $(GITHUB_BALL)
	$(MAKE) update-hash TARBALL=$(GITHUB_BALL)
	git push origin master
