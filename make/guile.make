GUILD := guild
GUILE_AUTO_COMPILE=0
GO_CACHE := $(OUT)/ccache

ifeq ($(GLP),)
GLP:=$(abspath $(shell pwd))/$(DIR):$(GUILE_LOAD_PATH)
GLCP:=$(GO_CACHE):$(GUILE_LOAD_COMPILED_PATH)
GUILE_LOAD_PATH:=$(GLP)
GUILE_LOAD_COMPILED_PATH:=$(GLCP)
endif
GUILD_FLAGS := $(patsubst %,-L %, $(subst :, ,$(GUILE_LOAD_PATH)))
ifeq ($(VERBOSE),)
$(info )
$(info GUILD_FLAGS=$(GUILD_FLAGS))
endif

SOURCES+=$(SCM_FILES)
SOURCES+=$(SCM_IN_FILES)

GO_PREVIOUS:=$(GO_FILES)
GO_FILES:=\
 $(SCM_FILES:$(DIR)/%.scm=$(GO_CACHE)/$(DIR)/%.go)\
 $(SCM_IN_FILES:$(DIR)/%.scm.in=$(GO_CACHE)/$(DIR)/%.go)\
#

ifeq ($(VERBOSE),)
$(info GO_PREVIOUS $(GO_PREVIOUS))
$(info GO_FILES $(GO_FILES))
$(foreach o,$(GO_FILES),$(eval $(o): $(GO_PREVIOUS)))

$(info SOURCES: $(SOURCES))
$(info SCM_FILES: $(SCM_FILES))
$(info SCM_IN_FILES: $(SCM_IN_FILES))
$(info GO_FILES: $(GO_FILES))
endif

CLEAN:=$(CLEAN) $(GO_FILES)

$(GO_CACHE)/$(DIR)/%.go: DIR:=$(DIR)
$(GO_CACHE)/$(DIR)/%.go: GUILD_FLAGS:=$(GUILD_FLAGS)
$(GO_CACHE)/$(DIR)/%.go: GUILE_LOAD_COMPILED_PATH:=$(GUILE_LOAD_COMPILED_PATH)
$(GO_CACHE)/$(DIR)/%.go: $(DIR)/%.scm
	@echo "GUILEC	$< -> $(notdir $@)"
	@mkdir -p $(dir $@)
	$(VERBOSE)\
		GUILE_AUTO_COMPILE=0\
		GUILE_LOAD_COMPILED_PATH=$(GUILE_LOAD_COMPILED_PATH)\
		$(GUILD) compile $(GUILD_FLAGS) -o $@ $<\
		| (grep -v ^wrote || true )

$(GO_CACHE)/$(DIR)/%.go: $(OUT)/$(DIR)/%.scm
	@echo "GUILEC	$<"
	@mkdir -p $(dir $@)
	$(VERBOSE)\
		GUILE_AUTO_COMPILE=0\
		GUILE_LOAD_COMPILED_PATH=$(GUILE_LOAD_COMPILED_PATH)\
		$(GUILD) compile $(GUILD_FLAGS) -o $@ $<\
		| (grep -v ^wrote || true )

include make/reset.make
