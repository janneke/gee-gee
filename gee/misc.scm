;;; Gee Gee --- Guile-Guided Gee-Guessing
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Gee Gee.
;;;
;;; Gee Gee is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Gee Gee is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gee Gee.  If not, see <http://www.gnu.org/licenses/>.

(read-set! keywords 'prefix)

(define-module (gee misc)
  :use-module (ice-9 and-let-star)
  :use-module (ice-9 curried-definitions)
  :use-module (ice-9 optargs)
  :use-module (ice-9 match)
  :use-module (ice-9 popen)
  :use-module (ice-9 rdelim)

  :export (
           ->string
           ->symbol
           gulp-file
           gulp-pipe
           gulp-port
           logf
           stderr
           stdout
           tuple<
           tuple<=
           ))

(define* ((->string :optional (infix "")) h . t)
  (let ((o (if (pair? t) (cons h t) h)))
    (match o
      ((? char?) (make-string 1 o))
      ((? number?) (number->string o))
      ((? string?) o)
      ((? symbol?) (symbol->string o))
      ((h ... t) (string-join (map (->string) o) ((->string) infix)))
      (_ ""))))

(define (->symbol o)
  (match o
    ((? char?) (->symbol (make-string 1 o)))
    ((? number?) o)
    ((? string?) (string->symbol o))
    ((? symbol?) o)
    (_ (->symbol ""))))

(define (gulp-file file-name)
  (with-input-from-file file-name read-string))

(define (gulp-pipe command)
  (let* ((port (open-pipe command OPEN_READ))
         (output (read-string port)))
    (close-port port)
    (string-trim-right output #\newline)))

(define (logf port string . rest)
  (apply format (cons* port string rest))
  (force-output port)
  #t)

(define (stderr string . rest)
  (apply logf (cons* (current-error-port) string rest)))

(define (stdout string . rest)
  (apply logf (cons* (current-output-port) string rest)))

(define (tuple< a b)
  (cond
   ((and (null? a) (null? b)) #t)
   ((null? a) (not (null? b)))
   ((null? b) #f)
   ((and (not (< (car a) (car b)))
         (not (< (car b) (car a))))
    (tuple< (cdr a) (cdr b)))
   (else (< (car a) (car b)))))

(define (tuple<= a b)
  (or (equal? a b) (tuple< a b)))
