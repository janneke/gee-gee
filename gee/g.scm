;;; Gee Gee --- Guile-Guided Gee-Guessing
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Gee Gee.
;;;
;;; Gee Gee is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Gee Gee is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gee Gee.  If not, see <http://www.gnu.org/licenses/>.

(read-set! keywords 'prefix)

(define-module (gee g)
  :use-module (srfi srfi-1)
  :use-module (srfi srfi-26)

  :use-module (ice-9 match)

  :use-module (gee misc)
  :use-module (gee git)
  :use-module (gee guix)
  :export (complete))

(define (seq? a b)
  (eq? (->symbol a) (->symbol b)))

(define (command args)
  (match args
         (((or "git" "guile" "guix") t ...) (string->symbol (car args)))
         (((? (cut member <> guix-commands seq?)) t ...) 'guix)
         (((? (cut member <> git-commands seq?)) t ...) 'git)
         (_ 'guile)))

(define (strip-command args)
  (match args
         (((or "git" "guile" "guix") t ...) (cdr args))
         (_ args)))

(define (complete args)
  (cons ((->string) (command args)) (strip-command args)))
