GIT_COMMANDS:=$(shell git help -a\
  | grep '^  [a-z]' | tr ' ' '\n' | sort | grep -v '^$$')
GUIX_COMMANDS:=$(shell guix --help\
  | grep -Ev '(pull)' \
  | grep '^   [a-z]' | tr ' ' '\n' | sort | grep -v '^$$')

$(OUT)/$(DIR)/%.scm: $(DIR)/%.scm.in
	@echo "SED	$< -> $(notdir $@)"
	@mkdir -p $(@D)
	@sed \
		-e 's,@GIT_COMMANDS@,$(GIT_COMMANDS),'\
		-e 's,@GUIX_COMMANDS@,$(GUIX_COMMANDS),'\
		-e 's,@PREFIX@,$(PREFIX),'\
		-e 's,@VERSION@,$(VERSION),'\
		$< > $@

SCM_IN_FILES:=$(filter %.scm.in,\
  $(shell test -d .git && (git ls-files $(DIR)) || find $(DIR)))
include make/guile.make

SCM_FILES:=$(filter %.scm,\
  $(shell test -d .git && (git ls-files $(DIR)) || find $(DIR)))
$(filter-out %misc.go,\
  $(SCM_FILES:$(DIR)/%.scm=$(CCACHE)/$(DIR)/%.go)): $(CCACHE)/$(DIR)/misc.go
include make/guile.make
