[[https://github.com/janneke/gee-gee.git/][Gee Gee]]

Guile-Guided Gee-Guessing for d33p n173 g17-n-g:iks h4x0rz

Gee Gee helps your poor limbic system get to grips
with all these newfangled source and package-based g* commands.

Gee Gee is best used on [[http://www.gnu.org/software/guix/][GNU GuixSD]]
or in combination with the guix package manager.

If this seems scarily familiar to you
#+BEGIN_EXAMPLE
    15:27:06 janneke@drakenvlieg:~/src/gee-gee
    $ git package -S 38
    git: 'package' is not a git command. See 'git --help'.
    15:27:07 janneke@drakenvlieg:~/src/gee-gee
    $ git build lilypond
    git: 'build' is not a git command. See 'git --help'.

    Did you mean this?
            bundle
    [1]15:27:33 janneke@drakenvlieg:~/src/gee-gee
    $ guix rebase -i HEAD~8
    guix: rebase: command not found
    Try `guix --help' for more information.
    [1]15:27:59 janneke@drakenvlieg:~/src/gee-gee
#+END_EXAMPLE
then Gee Gee may be for you.

* Requirements

Gee Gee currently depends on the following packages:

  - [[http://gnu.org/software/guile/][GNU Guile 2.0.x]], version 2.0.7 or later
  - [[http://www.nongnu.org/guile-lib//][guile-lib 0.2.2]], version 0.2.2 or later

* Hacking

To build gee-gee, I do

    make guix OPTIONS='--with-source=guile-2.0=guile-next --with-source=guile-lib=guile-next-lib'

* Contact

Gee Gee is hosted at [[https://github.com/janneke/gee-gee.gi/][Gee Gee]].

Please email <janneke@gnu.org> for bug reports or questions regarding
Gee Gee.

Join #guix on irc.freenode.net.
